\documentclass[11pt]{article}
% Command line: pdflatex -shell-escape compulse.tex
\usepackage{geometry} 
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{times}
\usepackage{bm}
\usepackage{fixltx2e}
\usepackage[outerbars]{changebar}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{color}
\usepackage{tabularx}
\usepackage{ulem}

\epstopdfsetup{suffix=} % to remove 'eps-to-pdf' suffix from converted images
%\usepackage{todonotes} % use option disable to hide all comments

\usepackage[sort&compress]{natbib}
\bibpunct{[}{]}{,}{n}{,}{,}

\usepackage[noend]{algpseudocode}

\usepackage{dsfont}
\usepackage{relsize}

%changing the Eq. tag to use [] when numbering. use \eqref{label} to reference equations in text.
\makeatletter
  \def\tagform@#1{\maketag@@@{[#1]\@@italiccorr}}
\makeatother

\linespread{1.5}
%\setlength{\parindent}{0in}

% the following command can be used to mark changes made due to reviewers' concerns. Use as \revbox{Rx.y} for reviewer x, concern y.
\newif\ifmarkedup
%\markeduptrue

\ifmarkedup
	\newcommand{\revbox}[1]{\marginpar{\framebox{\textcolor{blue}{#1}}}}
\else
	\newcommand{\revbox}[1]{}
	\renewcommand{\textcolor}[1]{}
	\renewcommand{\sout}[1]{}
\fi

%\newcommand{\bop}{$\vert B_1^+ \vert$}
\newcommand{\kt}{$k_\textrm{T}$}
\newcommand{\bmap}{$B_1^+$}
\newcommand{\mytilde}{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}  % tilde symbol
\mathchardef\mhyphen="2D

\begin{document}

\title{Ratio-adjustable power splitters for array-compressed parallel transmission and RF shimming}
\author{Xinqiang Yan$^{1,2}$, Zhipeng Cao$^{1,3}$, and William A. Grissom$^{*1,2,3,4}$}
\maketitle
\begin{flushleft}
$^1$Vanderbilt University Institute of Imaging Science, Nashville, TN, United States\\
$^2$Department of Radiology, Vanderbilt University, Nashville, TN, United States \\        
$^3$Department of Biomedical Engineering, Vanderbilt University, Nashville, TN, United States\\
$^4$Department of Electrical Engineering, Vanderbilt University, Nashville, TN, United States \\       

\par
-------------------------- 

\par
Word Count: Approximately X \\
*Corresponding author: \\
Will Grissom\\
Department of Biomedical Engineering\\
Vanderbilt University\\
5824 Stevenson Center\\
Nashville, TN 37235 USA \\
E-mail: will.grissom@vanderbilt.edu \\
Twitter: @wgrissom

\par Submitted to Magnetic Resonance in Medicine for consideration as a Note.

\par
Acknowledgment: This work was supported by NIH R01 EB016695, R01 DA019912, and R21 EB018521.

\end{flushleft}
\thispagestyle{plain}

\pagebreak

%%%%%%%%%%%%%%%%%%%%%%% Abstract %%%%%%%%%%%%%%%%%%%%%%%

\section*{Abstract} 
{\bf Purpose:}
To implement and validate a hardware-based array-compressed parallel transmission (acpTx) system.
\\
\\
{\bf Methods:}
In acpTx, a small number of transmit channels drive
a larger number of transmit coils, which are connected via an array compression network 
that implements optimized coil-to-channel combinations. 
A two channel-to-eight coil array compression network was developed using power splitters,
attenuators and phase shifters, 
and a simulation was performed to investigate the effects of coil coupling on power dissipation 
in a simplified network.
An eight coil transmit array was constructed using induced current elimination decoupling,
and the coil and network were validated in benchtop measurements, $B_1^+$ mapping scans, and an
accelerated spiral excitation experiment. 
\\
\\
{\bf Results:}
The developed attenuators came within 0.08 dB of the desired attenuations, 
and reflection coefficients were -22 dB or better.
The simulation demonstrated that 
up to 3$\times$ more power was dissipated in the network when coils were poorly isolated (-9.6 dB),
versus well-isolated (-31 dB). 
Compared to split circularly-polarized coil combinations, the additional degrees of freedom provided by the 
array compression network led to 54\% lower squared excitation error in the spiral experiment.
\\
\\
{\bf Conclusion:}
acpTx was successfully implemented in a hardware system.
Further work is needed to develop remote network tuning and to minimize
network power dissipation.
\\
\\

\noindent Key words: RF coils; MR engineering; ultra-high field MRI; RF pulse design; selective excitation; array compression; parallel transmission; optimization.

\pagebreak

%%%%%%%%%%%%%%%%%%%%%%% Main Text %%%%%%%%%%%%%%%%%%%%%%%

\section* {Introduction}
Many-coil transmit arrays are desirable in parallel transmission (pTx) 
\cite{Katscher:2003:Magn-Reson-Med:12509830, Zhu:2004:Magn-Reson-Med:15065251}, 
since with many coils multidimensional pulses can be shortened, 
more uniform radiofrequency (RF) shims can be produced, 
and specific absorption rate (SAR) can be more effectively controlled. 
However, the high cost and the large physical footprint and cabling requirements of the 
corresponding power amplifiers required to drive many-coil arrays has limited the 
number of transmit coils/channels that are used in practice, 
and most ultra-high field MR scanners in use today have only two or eight transmit channels. 
To overcome this limitation, array-compressed pTx (acpTx) was recently proposed \cite{cao:mrm:16,floser:ismrm15}
in which a large number of coils is connected to a small number of channels via an array compression 
network that splits the input pulse waveforms to the coils and applies attenuations and 
phase shifts that are optimized jointly with the pulse waveforms. 
While the concept has the potential to reduce the number of channels needed to drive many-coil arrays, 
to date it has only been implemented in simulations.

\par Here we report a hardware implementation of acpTx. 
A two channel-to-eight coil matrix transform circuit (comprising power splitters, attenuators and phase shifters) and an eight-coil head transmit array were developed for 7T. 
The hardware and its components were validated experimentally, 
including imaging scans using accelerated two-channel spiral acpTx excitation pulses.


\section*{Methods}

\subsection*{Attenuator design and validation}
An array compression network is a matrix transform circuit made up of power splitters, attenuators, phase shifters and (optionally) power combiners \cite{cao:mrm:16}. 
While power splitters and phase shifters can be constructed using standard methods (detailed below),
the required high-power attenuators are more challenging and to our knowledge have not been described previously for MRI applications. 
We developed an attenuator design comprising a Wilkinson power splitter and
combiner \cite{wilkinson:anwh:1960} connected by two transmission lines of different lengths,
which induce signal phase shifts of $\phi_1$ and $\phi_2$. 
Figure \ref{fig:atten}A shows a schematic of the circuit, and its
input-output voltage amplitude relationship is derived in the Appendix as:
\begin{equation}
\left\vert \frac{V_{out}}{V_{in}} \right\vert = \left\vert \cos \left( \frac{\phi_1 - \phi_2}{2}\right) \right\vert. 
\label{eq:ampatten}
\end{equation}
Equation \ref{eq:ampatten} indicates that the amount of attenuation has a cosine dependence on the 
difference between the transmission line lengths, and can thus be tuned by adjusting those lengths. 
The attenuator also applies a phase shift that depends on the transmission line lengths, 
and can be compensated by subsequent 
phase shifters in the array compression network.

\par A set of attenuators with attenuations between 1 dB and 7 dB in steps of 1 dB was built for validation
in 7T $B_1^+$ mapping experiments. 
The quarter-wave transmission lines in the Wilkinson power splitters/combiners were formed by a $\pi$ network 
comprising two parallel capacitors (7.5 pF, Passive Plus, 1111C Series, Huntington, NY, USA) and a handmade inductor ($\approx$38 nH). 
Figure 1B shows a constructed attenuator, 
in which the splitter and combiner were made on printed circuit boards with dimensions 3 $\times$ 3.5 cm$^2$. 
Two 50 $\Omega$ cables (RG-174, Belden Wire and Cable, Richmond, IN, USA) 
were used to change the relative phase shifts of the split signals and consequently the attenuation (Eq. \ref{eq:ampatten}). 
The attenuators' power limit is mainly determined by the resistors in their splitters and combiners.
The 100 $\Omega$ resistors used in the attenuators (American Technical Ceramics, high-power flange-mount resistor, Huntington Station, NY, USA) 
have a continuous wave power limit of 100 Watts
which is not expected to be a practical limitation, 
especially if suitable heat sinks are used.

\par To validate the attenuators, transmission and reflection coefficient measurements were made for each attenuator. 
They were then inserted between the RF amplifier of a Philips Achieva 7T scanner with 
two independent transmit channels (Philips Healthcare, Best, Netherlands) 
and the in-phase port of a birdcage head coil (Nova Medical, Wilmington, MA, USA). 
An axial $B_1^+$ map was acquired for each attenuator in a 15 cm gel phantom using the 
DREAM method \cite{nehrke:2012}.
As a baseline comparison an eighth scan was acquired with no attenuator in-line. 
The quadrature port was not driven, 
and the same input power and all other scan parameters were used for all scans. 
A Nova 32-channel receive-only coil was used for signal reception.

\subsection*{Array compression network}
Two network topologies have been proposed for acpTx: 
the $M$-to-$N$ topology, in which each coil receives a weighted copy of all channels’ pulses,
and the one-to-$N$ topology, 
in which each coil receives a weighted copy of only one of the input channels' pulses.
Compared with $M$-to-$N$ networks, one-to-$N$ networks offer fewer degrees of freedom but require many fewer components and no combiners, 
and it was shown in Ref. \cite{cao:mrm:16} that this hardware simplification comes with little cost in excitation accuracy, 
RF power and SAR. 
Figure \ref{fig:network}A illustrates a one-to-$N$ network with two input channels and eight coil outputs. 
A two channel-to-eight coil network was constructed based on this topology using six two-way power splitters,
eight attenuators and eight phase shifters (Figure \ref{fig:network}B). 
The constructed network has two stages: the first stage contains power splitters and phase shifters that
implement a `split CP' (circularly-polarized) two channel-to-eight coil network 
in which each transmit channel is connected to four consecutive coils in the array with
45 degree phase increments between them \cite{cao:mrm:16}, and the
second stage implements the desired acpTx attenuations and phase shifts. 
The power splitters were implemented using the same Wilkinson design used in the attenuators, 
and phase shifts were implemented using the same type of coaxial cables used in the attenuators. 
The specific values of the attenuations and phase shifts in the second stage of the network 
were determined from an acpTx spiral pulse design, described below.

\subsection*{Simulation of network power dissipation due to coil coupling}
In a conventional parallel transmission system, 
coupling among coil elements will lead to power loss since coupled power will be absorbed by circulators and wasted. 
In the proposed array compression network, 
the Wilkinson power splitters/combiners are designed to have isolated output/input ports with 50 $\Omega$ impedance,
and their resistors will absorb power coupled between coils.
To investigate the effect of coil coupling on power dissipation in a compression network, 
we numerically analyzed a pair of coupled coils connected to a simple one channel-to-two coil network.
Figure \ref{fig:sim}A illustrates the analysis steps:
\begin{enumerate}
\item First, simulations of a two-channel loop array were performed with and without decoupling treatment 
using HFSS (ANSYS, Canonsburg, PA, USA) \cite{kozlov:jmr:2009}. 
The two loops had identical dimensions of 15 $\times$ 7.5 cm$^2$ and were placed 3 cm apart. 
A water phantom (relative permittivity $\epsilon_r$ = 78; conductivity $\sigma$ = 0.6 S/m) with dimensions 45 $\times$ 25 $\times$ 20 cm$^3$ was placed 1 cm below the loops. 
To decouple one of the arrays, 
a small external loop (15 $\times$ 2.8 cm) was inserted between coil loops and was tuned to achieve the best possible isolation (Ref. \cite{yan:7ttr:2014}; further details below).

\item S-parameter matrices were exported from HFSS into an RF circuit simulator (ANSYS Designer, Canonsburg, PA, USA). 
All coils were tuned to 298 MHz and matched to 50 $\Omega$ ($S_{11}$ = -30 dB). 
The element-to-element isolation of coupled and decoupled coils was -9.6 dB and -31 dB, respectively.

\item The additional power dissipation in the network was calculated from the circuit simulation,
i.e., the total power deposited in the network's three resistors excluding the expected dissipation due to attenuation, 
which without coil coupling would be zero.
By adjusting the transmission line lengths in the attenuator, 
the relative input-output amplitude ratio was varied from 0 to 1 with a step of 0.2 and 
the relative phase was varied from 0 to 360 degrees with a step of 1 degree.
\end{enumerate}


\subsection*{Transmit coil array}
Multiple decoupling methods for transmit coil arrays have been developed in previous studies
\cite{adriany:tart:2005,pinkerton:mrm:2005,alagappan:2007,adriany:mrm:2010,shajan:mrm:2014,li:medphys:2011,yan:7ttr:2014}.
In this work, a gapped loop array design was used with the induced current 
elimination (ICE) decoupling method \cite{li:medphys:2011,yan:7ttr:2014}, 
which has been demonstrated to perform well in both heavy and light loading scenarios.

\par The transmit coil was constructed on an acrylic tube with outer diameter 30 cm and length 25.4 cm. 
The rectangular loop elements had dimensions 17 $\times$ 9 cm$^2$. 
A total of eight fixed capacitors (3.9 pF, Passive Plus, 1111C Series, Huntington, NY, USA) 
and two variable capacitors (one for tuning and the other for matching, Johanson Manufacturing, 52H Series, Boonton, NJ, USA) were equally distributed in each loop. 
All loops were tuned to 298 MHz and matched to 50 $\Omega$. 
A shielded cable trap was used for each loop to avoid ``cable resonance''. 
Small ICE decoupling loops (17 $\times$ 2.8 cm$^2$) with three fixed capacitors and 
one variable capacitor were placed between adjacent coil loops (Figure \ref{fig:coil}A).
The coil was used for transmission only, 
together with the Nova 32-channel receive-only coil which fit inside it.
To actively detune the coil during receive, 
a PIN diode in series with an inductor was connected in parallel with one fixed capacitor in each loop. 
The distance between the transmit coil and the outer shielding of the receive coil
was approximately 2.5 cm.
The transmit coil was positioned to overlap with the receive array to 
further improve their isolation.
Figure \ref{fig:coil}B shows the constructed coil array.
Each coil's $B_1^+$ map was measured in the 15 cm gel phantom for spiral pulse design (described below) using the DREAM method, 
with the other coils terminated to 50 $\Omega$.


\subsection*{Spiral acpTx experiment}
To validate the array compression network and transmit coil array in an acpTx imaging experiment, 
accelerated spiral-in acpTx pulse waveforms 
and attenuations and phase shifts for the compression network were jointly
designed to excite a logo pattern.
For pulse design the coil array was split circumferentially into two sets of four coils,
assigning four consecutive coils to each of the network's two transmit channels.
The pulses and channel-to-coil weights were then designed using the iterative acpTx spiral pulse design method in Ref. \cite{cao:mrm:16}, 
which repeatedly alternates between reducing
a cost function comprising a squared-excitation error term \cite{Grissom:2006:MRM} 
and a small waveform roughness penalty \cite{grissom2010minimum}, 
and applying singular value truncation to constrain the rank of each of the two 
transmit channels' RF waveform matrices to one.
Further details on this pulse design method are provided in Ref. \cite{cao:mrm:16}.

\par The output of the pulse design was a pair of rank-one matrices $\hat{\bm{B}}_1$ and $\hat{\bm{B}}_2$
whose columns contained the four RF waveforms for each of the two transmit channels.
The singular value decomposition $\hat{\bm{B}}_i = \bm{U\Sigma V}^H$ of each matrix was 
calculated,
and the array compression weights were taken to be elements 
of the first columns of the right-singular vector ($\bm{V}^*$) matrices,
where $^*$ denotes complex conjugation of each matrix element.
The two transmit channels' RF pulses were taken to be the first columns of the $\bm{U\Sigma}$ matrices. 
The weights were normalized in amplitude by the weight with the largest amplitude,
and were implemented in the attenuators and phase shifters of the array compression network.
The implemented attenuations and phase shifts were checked using a network analyzer, 
and $B_1^+$ maps were measured for each of the two compressed transmit channels.
RF waveforms were multiplied by the inverse of the weight amplitude normalization to maintain the target
flip angle.


\par A 6.7 ms slew rate-limited spiral-in trajectory was used for pulse design with excitation-FOV 5 cm (3.3$\times$ acceleration in the 15-cm phantom) and maximum slew rate 120 mT/m/s. 
For comparison to the acpTx pulses, 
two-channel transmit pulses using the same spiral trajectory and pulse design cost function were designed 
for the split CP coil combinations implemented in the first stage of the hardware network
as described above. 
The pulses' excitation patterns were imaged using a 3D gradient echo sequence with
TE/TR = 2/1000 ms, flip angle = 5 degrees, FOV = 20$\times$20$\times$17.6 cm$^3$, 
voxel size = 3$\times$3$\times$10 mm$^3$, 
2$\times$ SENSE acceleration in anterior-posterior and foot-head directions. 


\section*{Results}

\subsection*{Attenuator validation}
Supplemental Figure \ref{fig:attenplots} shows reflection and transmission coefficient plots for each of the attenuators.
All were within 0.08 dB of the intended attenuation, and were well matched to 50 $\Omega$,
with reflection coefficients of -22 dB or better. 
Figure \ref{fig:atten}C shows normalized $B_1^+$ maps measured with attenuations from 0 dB (no attenuator) to 7 dB. 
Figure \ref{fig:atten}D plots predicted (using $S_{12}$ measurements of the constructed attenuators) 
and measured average $B_1^+$ at each attenuation,
both normalized by measured average $B_1^+$ with no attenuation.
The measured and predicted ratios match well, 
with errors less than 5\%.


\subsection*{Simulation of network power dissipation due to coil coupling}
Figure \ref{fig:sim}B plots power dissipation in the compression network as a function of 
relative amplitudes and phase shifts for the coupled and decoupled arrays, with 1 W input power. 
For the decoupled array, the additional power dissipation is less than 0.07 W across all attenuations and phase shifts. 
For the coupled array, however, the additional power dissipation is much larger (up to 0.21 W) and depends strongly on the 
attenuations, and less strongly on the phase shift. 


\subsection*{Transmit coil array}
Figure \ref{fig:coil}C shows the coil array's S-parameter matrix 
when loaded with the 15 cm gel phantom. %and Figure 3C shows its transmission response
%plots when it is loaded with the 15 cm gel phantom. 
Adjacent coil elements had a high isolation of -30 dB, 
and all other isolations were -14 dB or better. 
The isolation between any transmit coil and any receive coil was better than
-35 dB in both transmit and receive modes.
Figures \ref{fig:coil}D and E show each coil's $B_1^+$ map (amplitude and relative phase) in the phantom.
The magnitude maps have the characteristics of a single high frequency loop coil in isolation,
further reflecting the good isolation between coils. 
These maps were used in the spiral pulse designs.

\subsection*{Spiral acpTx experiment}
The final pulse design cost function values were 88.2 (mean squared error = 81, integrated waveform roughness = 7.2) for acpTx, 
and 187 (mean squared error = 175, integrated waveform roughness = 12) for split CP, in arbitrary units.
%Thus, the two solutions
%had similar roughness but the acpTx solution's squared error was 54\% lower than the split CP solution's error. 
Figure \ref{fig:spiralexp}A plots the intended and measured attenuations and phase shifts implemented in the array compression network for both configurations.
Compared to the intended acpTx weights, 
the measured weights have average/maximum amplitude errors of 2\%/4.8\% and average/maximum phase errors of 2.7/6.3 degrees. 
Figure \ref{fig:spiralexp}B shows the predicted and measured $B_1^+$ maps for the two channels of the split CP and acpTx combinations, 
and their difference.
Measured maps were very similar to the predicted maps in both cases,
with 12.5\% (split CP) and 14.5\% (acpTx) average error. 
Furthermore, most of the large $B_1^+$ errors appear near the edges of the maps, 
suggesting that they may largely result from imaging artifacts.
Figure \ref{fig:spiralexp}C shows the predicted and measured spiral excitation patterns. 
The split CP excitation had higher errors in both the predicted and measured patterns 
(some of which are indicated by the green arrows), 
and there is good agreement between the predicted and measured patterns in both cases.
The lower error and roughness of the acpTx excitation required a tradeoff of 3$\times$ 
higher total input power to the network from the
two transmit channels compared to split CP.
However, most of that additional power was dissipated in the network so 
the total power output to the coils was similar between the two cases, 
with 20\% higher output in the acpTx case.  
Overall, for acpTx 59\% of the input power was dissipated in the network. 


\section* {Discussion and Conclusion}

\subsection*{Summary of results}
This work presented a first experimental implementation of array-compressed parallel transmission,
in which a hardware compression network was built to implement coil combination
weights that were designed jointly with parallel RF pulses, using the pulse design method described 
in Ref. \cite{cao:mrm:16}. 
In building the network we developed and validated tunable attenuators with low reflection coefficients
based on Wilkinson power splitters and combiners connected by variable coaxial cable lengths.
A two channel-to-eight coil array compression network based on a previously-described one-to-$N$ topology \cite{floser:ismrm15,cao:mrm:16}
was built using the attenuators,
and phase shifts were implemented using variable coaxial cable lengths. 
A simulation was performed to investigate how electromagnetic coil coupling 
causes power dissipation in the network to increase beyond the level expected for attenuation.
The results showed that poor coil isolation can cause significant increases in 
network power dissipation (up to 3$\times$ higher in our simulation), 
and that the additional dissipation depends strongly on the relative signal attenuation between the 
two coils, and less so on their relative phase.
However, 
it is possible that a stronger dependency on relative phase could arise when there are more than two coils, 
due to interference between coupled coils.
These results underscored the need for good transmit coil isolation, 
so an eight-coil transmit array was constructed for the acpTx experiment using ICE decoupling. 
%Additional power dissipation in the network due to coil coupling could also be avoided by placing 
%circulators between the network and coil, which would absorb coupled power.
Finally, the coil array and compression network were validated in an accelerated spiral acpTx excitation,
and were compared with a two-channel spiral excitation using split CP coil combinations,
where it was found that the additional degrees of freedom provided by optimized coil combinations 
in acpTx led to lower excitation error and waveform roughness than for the split CP case, 
and that the network and coil performed as expected. 

\subsection*{RF Power dissipation and alternative network topologies}
In our spiral acpTx experiment, most (59\%) of the power from the RF amplifiers was 
dissipated in the array compression network, 
and compared to the split CP case the acpTx excitation required significantly higher input power (3$\times$ higher).
This was not problematic for the low-flip angle excitation experiment performed in this study,
but such a large power loss would likely limit other acpTx applications.
The amount of power loss depends strongly on the network topology; 
our network split each transmit channel four ways 
before applying any attenuation. 
This construction was chosen because it enabled a comparison to split CP excitation,
which used the same bank of power splitters as the acpTx network. 
However, alternative acpTx network topologies are possible which would incur substantially lower 
power dissipation; some are presented in Supplemental Figure \ref{fig:altern}. 
These networks use multiple signal paths with different numbers of power splitters,
so that relative signal amplitudes are varied by power splitting in addition to attenuation, 
thereby reducing the overall attenuation requirements.
Figure \ref{fig:altern} shows example networks that implement the same coil combinations implemented in
our constructed network, but with only 25\% power dissipation. 
%Note that RF power was not regularized in the design. 
%Solution is to use power splitters with more than two outputs, 
%and use variable numbers of power splitters along different signal paths. 

\subsection*{Remote network tuning}
In this work, attenuations and phase shifts in the array compression network were both adjusted using
different lengths of coaxial cable. 
While such a construction is feasible for a fixed network, subject-to-subject or scan-to-scan 
tuning of the array compression weights would require the ability to remotely adjust the weights
electronically or mechanically. 
Previous work in MRI has demonstrated the potential for remote tuning of phase shifts for RF shimming
using commercial motor-driven phase shifters \cite{metzger:mrm:2008},
and remote tuning and matching of coils using PIN diode switching of capacitor banks \cite{Sohn:doae:2015}, 
varactors \cite{venook:mrm:2005}, and piezoelectric actuators connected to variable capacitors \cite{keith:mrm:2015}.
Other electronic approaches to remotely adjust phase shifts include PIN diode-based switched-line shifters \cite{garver:bbdp:1972,white:dpsf:1974} and varactor-terminated transmission lines \cite{koul:1991}.
Future work will focus on developing remotely-adjustable array compression networks,
which will likely use electronic approaches rather than mechanical due to the need for a small footprint
and MR compatibility. 

%\section* {Conclusion}


\section* {Appendix}
\subsection* {Attenuator input-output relationship}

The two-way Wilkinson power splitters and combiners used in the attenuator (Figure 1A) have scattering matrix:
\begin{equation}
\bm{S} = \frac{-j}{\sqrt{2}} \left[ \begin{array}{ccc} 0 & 1 & 1 \\ 1 & 0 & 0 \\ 1 & 0 & 0 \end{array} \right]
\end{equation} 
As shown in the Figure, the input signal to the attenuator ($V_{in}$) is split into two signals 
$V_{A}$ and $V_{B}$
with equal amplitudes and phases which are derived from $\bm{S}$ to be:
\begin{equation}
V_{A} = V_{B} = \frac{-j}{\sqrt{2}} V_{in}.
\end{equation}
After phase delays ($\phi_1$ and $\phi_2$) applied by the transmission lines connecting the 
splitter and combiner, 
the signals at the inputs of the combiner are:
\begin{align}
V_{C} &= \frac{-j}{\sqrt{2}} V_{in} e^{-j\phi_1} \\
V_{D} &= \frac{-j}{\sqrt{2}} V_{in} e^{-j\phi_2}.
\end{align}
$V_{C}$ and $V_{D}$ are then combined to form the attenuated output signal $V_{out}$:
\begin{align}
V_{out} 	&= \frac{-j}{\sqrt{2}} \left(V_C + V_D\right) \\
		&= -\frac{1}{2} V_{in} \left(e^{-j \phi_1} + e^{-j \phi_2}\right) \\ 
		&= -\frac{1}{2} V_{in} \left(e^{-j \frac{\phi_1 - \phi_2}{2}} + e^{j \frac{\phi_1 - \phi_2}{2}} \right) e^{-j \frac{\phi_1 + \phi_2}{2}} \\
		&= -V_{in} \cos \left( \frac{\phi_1 - \phi_2}{2}\right) e^{-j \frac{\phi_1 + \phi_2}{2}}.
\label{eq:vout}
\end{align}
Equation \ref{eq:vout} shows that the amplitude ratio of $V_{out}$ and $V_{in}$ can be 
adjusted via the relative lengths of the transmission lines, according to:
\begin{equation}
\left\vert \frac{V_{out}}{V_{in}} \right\vert = \left\vert \cos \left( \frac{\phi_1 - \phi_2}{2}\right) \right\vert.
\label{eq:ampratio}
\end{equation}
Equation \ref{eq:vout} also shows that the attenuator applies a phase shift that depends on $\phi_1$ and $\phi_2$, 
which can be compensated later in the array compression network.

\pagebreak

\bibliographystyle{cse}
\bibliography{acpTx_hardware}

\pagebreak

\begin{figure}
\caption{Attenuator design and construction. 
(A) Schematic of the proposed attenuator, comprising a Wilkinson power splitter and a Wilkinson combiner which 
are connected by two transmission lines of different lengths.
(B) A constructed attenuator. 
(C) $B_1^+$ maps acquired 
with different attenuators inserted between the RF amplifier and the in-phase port of a birdcage coil. 
(D) Ratios of average $B_1^+$ at each attenuation, normalized to zero attenuation.}
\label{fig:atten}
\end{figure}

%\begin{figure}
%\caption{Attenuator validation results. 
%(A) $B_1^+$ maps acquired 
%with different attenuators inserted between the RF amplifier and the in-phase port of a birdcage coil. 
%(B) Ratios of average $B_1^+$ at each attenuation, normalized to zero attenuation.}
%\label{fig:attenresults}
%\end{figure}

\begin{figure}
\caption{Array compression network design and construction. 
(A) Schematic of a two channel-to-eight coil array compression network, 
based on a one-to-$N$ topology in which each coil receives a weighted copy of only one of the
input channels' pulses.
(B) The constructed network based on this design, which was used in the spiral acpTx experiment.
The network has two stages: a split CP stage (left) which splits each of the two input channels
to four coils, with 45 degree phase increments in-between, and an attenuation and phase shift 
stage (right) which applies the acpTx compression weights.
The attenuators used connectorized transmission lines for simpler adjustment.}
\label{fig:network}
\end{figure}

\begin{figure}
\caption{Simulation of network power dissipation with and without coil coupling.
(A) The analysis procedure: First, two-channel loop arrays with and without decoupling treatment (red arrow) were simulated.
Then the S-parameter matrices were imported into an RF circuit simulator,
and the additional power dissipation (compared to zero coil coupling) in the three resistors of the network 
were calculated over a range of relative amplitudes and phase shifts. 
(B) Plots of additional power dissipation in the circuit as a function of relative amplitudes (0 to 1 with a step of 0.2) and phase shifts (0 to 360 degree with a step of 1 degree) between the coils.}
\label{fig:sim}
\end{figure}

\begin{figure}
\caption{Transmit coil array. 
(A) Diagram of two coil loops and an ICE decoupling loop. 
(B) The constructed eight-coil transmit array. 
(C) S-Parameter matrix of the 8-coil array when loaded with a 15-cm gel phantom. 
(D,E) Amplitudes (D) and relative phases (E) of the coils' $B_1^+$ fields in an axial slice. }
\label{fig:coil}
\end{figure}

\begin{figure}
\caption{Spiral acpTx experiment. 
(A) Intended and measured relative amplitudes and phase shifts applied by the split CP and acpTx two channel-to-eight coil networks.
Coils 1-4 were connected to channel one, and coils 5-8 were connected to channel two. 
(B) Predicted and measured $B_1^+$ amplitude maps for the two transmit channels in split CP and acpTx configurations, and the differences between the predicted and measured maps.
(C) Simulated excitation patterns and errors, and measured images for the split CP and acpTx 
excitations.
Green arrows indicate excitation errors that are absent or have smaller amplitude in the acpTx pattern.}
\label{fig:spiralexp}
\end{figure}

\renewcommand\thefigure{S\arabic{figure}} 
\setcounter{figure}{0}  

\begin{figure}
\caption{Reflection and transmission coefficient plots for each attenuator (1 dB to 7 dB). 
In all cases, attenuations were within 0.08 dB of the intended value and the input and output ports were well matched to 50 $\Omega$,
with reflection coefficients lower than -22 dB at 298 MHz.}
\label{fig:attenplots}
\end{figure}

\begin{figure}
\caption{Alternative array compression network topologies that implement the same (or approximately the same, as indicated) 
compression weights as in the spiral acpTx experiment, but with lower power dissipation. 
Splitter boxes report the factor by which each output voltage is reduced compared to the input.
Grey boxes indicate attenuators and the amount of attenuation. 
Phase shifters were omitted for brevity.
The total input power dissipation in the network is reported above each network.}
\label{fig:altern}
\end{figure}




\end{document}